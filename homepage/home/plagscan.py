from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from time import sleep
from selenium.webdriver.common.action_chains import ActionChains
import pdb
import os
from os.path import dirname, realpath
from selenium.common.exceptions import NoSuchElementException

# username = 'fonatividad@pup.edu.ph'
# password = '79dBeoAh'
user = ""
pwd = ""

def login_user(username, password):
    try:
        global user 
        global pwd
        user = username
        pwd = password
        # browser = webdriver.Firefox(dirname(realpath(__file__)) + '\geckodriver.exe')        
        browser = webdriver.Chrome(dirname(realpath(__file__)) + '\chromedriver.exe')
        # browser = webdriver.Firefox()    
        # browser = webdriver.PhantomJS(dirname(realpath(__file__)) + '\phantomjs.exe')
        print(dirname(realpath(__file__)))
        browser.get('https://www.plagscan.com/pup')
        # fill in username and hit the next button
        sleep(2)
        print('Finding username/password field ....')
        userElement = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.ID, 'UserEmail')))
        username_field = browser.find_element_by_id('UserEmail')
        username_field.send_keys(username)
        password_field = browser.find_element_by_id('UserPass')
        password_field.send_keys(password)
        signInButton = browser.find_element_by_id('btn-login')
        signInButton.click()
        sleep(5)
        try:
            print('Signed In. Finding upload btn ...')
            sleep(5)
            uploadBtn = browser.find_element_by_xpath("//input[@id='fileInput']")
            browser.quit
            return True
        except:
            browser.quit()
            print('Failed to login: Invalid Username and Password!')
            return False
    except NoSuchElementException:
        browser.quit()
        print('Failed to login: Check your internet connection!')
        return False

def plagscan_upload(content):
    try:
        # browser = webdriver.Firefox()
        # browser = webdriver.PhantomJS(dirname(realpath(__file__)) + '\phantomjs.exe')
        browser = webdriver.Chrome(dirname(realpath(__file__)) + '\chromedriver.exe')
        browser.get('https://www.plagscan.com/pup')
        print('Finding username/password field ....')
        userElement = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.ID, 'UserEmail')))
        username_field = browser.find_element_by_id('UserEmail')
        username_field.send_keys(user)
        password_field = browser.find_element_by_id('UserPass')
        password_field.send_keys(pwd)
        sleep(5)
        browser.find_element_by_id('btn-login').click()
        print('hello')
        uploadElement = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.ID, 'flashLink')))
        try:
            print('Signed In.')
            uploadBtn = browser.find_element_by_xpath("//input[@id='fileInput']")
            print('Found Upload Button')
            uploadBtn.send_keys(content)
            print('Clicked Upload Button')
            sleep(5)
            process = WebDriverWait(browser, 10).until(EC.invisibility_of_element_located((By.CLASS_NAME, 'fa-circle-o-notch')))
            print('DONE')
            sleep(2)
            checkBtnElement = WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'a.btn.btn-block.btn-sm.btn-default')))
            checkBtn = browser.find_element_by_xpath("//tbody[@id='newUploads']//a[@class='btn btn-block btn-sm btn-default']")
            checkBtn.click()
            # TRY AND EXCEPT FOR RESULT
            return True
        except NoSuchElementException:
            browser.quit()
            print('Failed to login: Invalid Username and Password!')
            return False
        except TimeoutException:
            browser.quit()
            print('Timeout Exception')
            return False
    except NoSuchElementException:
        print('Failed to login')
        browser.quit()
        return False