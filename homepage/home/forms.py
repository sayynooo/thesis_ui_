from django import forms
from .models import Document

# class DocumentForm(forms.Form):
#     doc_title = forms.CharField(max_length=250)
#     doc_type = forms.CharField(max_length=10)
#     doc_pages = forms.IntegerField()

class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('document',)

    # def __str__(self):
        # return "Title: %s, Type: %s, Pages: %d" % (self.doc_title, self.doc_type, self.doc_pages)