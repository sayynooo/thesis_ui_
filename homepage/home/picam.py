import time, os
from picamera import PiCamera

def capture_images(path, page_no):

    camera = PiCamera()
    for i in range(1, (page_no*2+1)):
        camera.start_preview()
        time.sleep(10)
        image_name = 'image{0:02d}.jpg'.format(i)
        os.chdir(path)
        camera.capture(image_name)
        print("Captured image", i)
