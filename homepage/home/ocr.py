import pytesseract
from PIL import Image

def read_handwritten_img(image_path):
    pass

def read_typewritten_img(image_path):
    return pytesseract.image_to_string(Image.open(image_path))
